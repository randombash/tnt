#import base image
FROM alpine:latest

#in case i'll ever use it again?
MAINTAINER Marta Kubis <ktext007@gmail.com>

# Set the working directory to root
WORKDIR /usr/local/ip_info

# Copy the current directory (dev dir) contents  into the container at workingdir
COPY LICENSE  README.md  dependencies ip_info ./

# Install task script's dependencies
RUN apk update && apk add  $(cat dependencies | xargs)

#add the script to PATH so it's available as a command
COPY ip_info /usr/bin

# assign TCP port 43 (private,image) to 43000 (public,host)
EXPOSE 43

#launch bash shell
ENTRYPOINT ["bash"]





